package br.edu.calc.plus.selenium;

import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@ActiveProfiles("test")
@SpringBootTest
public class TestLogin {
  private WebDriver driver;
  //private Map<String, Object> vars;
  //JavascriptExecutor js;

  @BeforeEach
  public void setUp() {
    System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
    driver = new ChromeDriver();
    //js = (JavascriptExecutor) driver;
    //vars = new HashMap<String, Object>();
  }

  @AfterEach
  public void tearDown() {
    driver.quit();
  }

  @Test
  public void testaLoginErrado() throws Exception {
    driver.get("http://localhost:9000/login");
    driver.manage().window().setSize(new Dimension(993, 632));
    driver.findElement(By.id("username")).sendKeys("rick");
    driver.findElement(By.id("password")).sendKeys("123456");
    driver.findElement(By.cssSelector(".btn")).click();
    String txt = driver.findElement(By.cssSelector(".alert > span")).getText();
    System.out.println("TEXTO: ");
    System.out.println(txt);
    assertTrue(txt == "Login ou senha incorreta");
  }
} 